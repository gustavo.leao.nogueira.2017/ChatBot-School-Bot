from chatterbot import *
from chatterbot.trainers import *
from sys import argv, exit

class SchoolBot:
    bot = ChatBot("School bot")
    def inicializar(self):
        self.bot.set_trainer(ListTrainer)
        self.bot.train([
        "Oi, tudo bem?",
        "Sim e com você?",
        "Bem.",
        "Qual seu nome?",
        "School bot.E o seu?",
        "Prazer!",
        "Quantos anos você tem?",
        "Um e você?",
        "Hum."
        ])
    def mensagem(self):
        self.textoAnteriror = self.caixaDeTexto.toPlainText()
        self.valorDigitado = self.entradaDeTexto.text()
        self.resposta = self.bot.get_response(self.valorDigitado)
        self.caixaDeTexto.setText("{}\n Você: {}\nComputador: {}\n".format(self.textoAnteriror, self.valorDigitado, self.resposta))

    def __init__(self):
        self.inicializar()
    
        self.app = QApplication(argv)
        
        self.horizontal = QHBoxLayout()
        self.vertical = QVBoxLayout()
        
        self.texto = QLabel("Mensagem:")
        self.caixaDeTexto = QTextEdit()
        self.entradaDeTexto = QLineEdit()
        self.botao = QPushButton("Enviar")
        self.botao.clicked.connect(self.mensagem)
        
        self.horizontal.addWidget(self.texto)
        self.horizontal.addWidget(self.entradaDeTexto)
        self.horizontal.addWidget(self.botao)
        
        self.vertical.addWidget(self.caixaDeTexto)
        self.vertical.addLayout(self.horizontal)
        
        self.janela = QWidget()
        self.janela.setWindowTitle("School Bot")
        self.janela.resize(800,600)
        self.janela.setLayout(self.vertical)
        self.janela.show()
        
        exit(self.app.exec_())
        
if __name__ == '__main__':
    SchoolBot()